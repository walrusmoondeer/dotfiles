#!/usr/bin/env sh 
menu_text=" Wayland Config Files\n wayfire.ini\n wofi/config\n wofi/style.css\n waybar/config\n waybar/style.css\n swaylock/config\n newsboat/urls"
lines=9
terminal="alacritty"
if [ $XDG_SESSION_TYPE = "x11" ]; then
  chosen=$(printf "${menu_text}" | rofi -dmenu -theme ericmurphyxyz -theme-str "listview {lines: ${lines};}" -matching prefix)
else
  chosen=$(printf "${menu_text}" | wofi -d -L "${lines}" ) 
fi
case "$chosen" in 
  " wayfire.ini")      $terminal -e nvim $HOME/.config/wayfire.ini ;;
  " wofi/config")      $terminal -e nvim $HOME/.config/wofi/config ;;
  " wofi/style.css")   $terminal -e nvim $HOME/.config/wofi/style.css ;;
  " waybar/config")    $terminal -e nvim $HOME/.config/waybar/config ;;
  " waybar/style.css") $terminal -e nvim $HOME/.config/waybar/style.css ;;
  " swaylock/config")  $terminal -e nvim $HOME/.config/swaylock/config ;;
  " newsboat/urls")    $terminal -e nvim $HOME/.config/newsboat/urls ;;
  *) exit 1 ;;
esac
