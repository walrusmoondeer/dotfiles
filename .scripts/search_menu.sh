#!/usr/bin/env sh 
if [ $XDG_SESSION_TYPE = "x11" ]; then
  rofi -show drun -font "hack 10" -icon-theme "Papirus" -show-icons -theme docu -modi drun -drun-categories Development,Education,Game,Graphics,Office,Utility,Email,Spreadsheet,WordProcessor,Presentation,InstantMessaging,Chat,FileTransfer,News,P2P,WebBrowser,Midi,Music,IDE,AudioVideoEditing,Player,ActionGame,AdventureGame,ArcadeGame,BoardGame,BlocksGame,CardGame,KidsGame,LogicGame,RolePlaying,Simulation,SportsGame,StrategyGame,Emulator,FileManager,TerminalEmulator,Security,TextEditor
else
  #fuzzel -b 222222ff -s 333333ff -l 20 
  wofi -W 40% -w 2 -I --show=drun 
fi
### List of Desktop Application Categories: ###
# https://askubuntu.com/questions/674403/when-creating-a-desktop-file-what-are-valid-categories
