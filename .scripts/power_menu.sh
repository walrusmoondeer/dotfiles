#!/usr/bin/env sh 
power_menu_text="\uf011  shutdown\n\uf021  reboot\n\uf456  lock-screen\n\uf426  log-out"

if [ $XDG_SESSION_TYPE = "x11" ]; then
  chosen=$(printf "${power_menu_text}" | rofi -dmenu -theme ericmurphyxyz -matching prefix -theme-str 'listview {lines: 4;}')
  # TODO: logout= awesome-client 'awesome.quit'
else
  #chosen=$(printf "${power_menu_text}" | fuzzel -b 222222ff -s 333333ff -l 4 -d )
  chosen=$(printf "${power_menu_text}" | wofi -L 5 -d )
  # NOTE wofi: can't remove searchbar space (can do horizontal icons, but will have shit space)
  #logout=$(wlogout) # <-- not installed
fi

# $DESKTOP_SESSION <-- get current window manager name
case "$chosen" in 
  *shutdown) systemctl poweroff ;;
  *reboot) systemctl reboot ;;
  *lock-screen) bash $HOME/.scripts/lockscreen.sh ;;
  *log-out) wlogout;;
  *) exit 1 ;;
esac
