#!/usr/bin/env sh 
if [ $XDG_SESSION_TYPE = "x11" ]; then
  rofi -show run -theme ericmurphyxyz
else
  wofi --show=run 
fi
