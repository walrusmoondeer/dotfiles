#!/usr/bin/env sh 

# NOTE: lockscreen doesn't work on qtile (wayland)
if [ $XDG_SESSION_TYPE = "x11" ]; then
  env XSECURELOCK_SAVER=saver_xscreensaver XSECURELOCK_PASSWORD_PROMPT=asterisks xsecurelock
else
  #swaylock -LFe -s fill -i $HOME/.local/share/backgrounds/misty_epic_mountain.jpg
  swaylock
fi
