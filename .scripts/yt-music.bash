#!/usr/bin/env bash
if [ -z $1 ]; then echo "Dont forget the URL!"; exit; fi
jq --version > /dev/null || (echo "This script depends on 'jq'!"; exit)
mkdir -p $HOME/Music 
#yt-dlp --dump-json "$1" | jq length | wc -l # amount of JSON objects (songs) in playlist 
if [[ $(yt-dlp --dump-json "$1" | jq '.artist') == *"null"* ]]; then # if any song has 'null' for artists, then all songs will have titles
  format="%(title)s"
  json_format=".title, .id"
else 
  format="%(artist)s - %(alt_title)s"
  json_format=".artist + .alt_title, .id" # there is no space between .artist and .alt_title, and I dont know how to get one
fi
yt-dlp --dump-json "$1" | jq "${json_format}" >> $HOME/Music/.downloaded-music-list.txt
yt-dlp --extract-audio --audio-format vorbis --audio-quality 4 --embed-thumbnail --convert-thumbnails jpg -o "$HOME/Music/${format}.%(ext)s" "$1"
# NOTES:
# back in 2015 youtube didnt store audio at better then 156 ACC
# the changing of audio quality and thumbnail format has made an almost 3 time improvement in storage space
# webp is more efficient then png and JPG, but when i tried webp it just went to PNG, where as JPG works, thats why i chose JPG
# if i continue having audio issues with .ogg files, then i should use 'best' quality (mp3) instead.

RED='\033[0;31m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color
if [ $format == "%(title)s" ]; then 
  echo -e "${RED}Music uses youtube titles for name!${NC}"
else 
  echo -e "${BLUE}Music uses artists and alt_titles for name!${NC}"
fi 