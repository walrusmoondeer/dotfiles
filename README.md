# Dotfile overview:
- **Distro:** Archlinux
- **WM:** Wayfire (Wayland)
- **Login screen:** SDDM
- **Shell:** zsh 
- **Dotfile management:** yadm
- **Terminal:** Alacritty
- **Editor:** Neovim (using a template: github.com/LunarVim/nvim-basic-ide)
- **Launcher (Wayland):** Wofi
- **Fetch:** Fastfetch (Neofetch clone written in C)
- **File manager:** PCManFM-gtk3

last commit having xorg configs:
```bash
eec1fc3
```
