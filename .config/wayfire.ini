# Take the tutorial to get started.
# https://github.com/WayfireWM/wayfire/wiki/Tutorial
#
# Read the Configuration document for a complete reference.
# https://github.com/WayfireWM/wayfire/wiki/Configuration

# TODO: brighness, redshift
# TODO: wallpaper switcher: https://sylvaindurand.org/dynamic-wallpapers-with-sway/
# Input configuration ──────────────────────────────────────────────────────────

# Example configuration:
#
[input]
xkb_layout = lv
# xkb_variant = dvorak,bepo
# xkb_options = grp:win_space_toggle

# Output configuration ─────────────────────────────────────────────────────────

# Example configuration:
#
# [output:eDP-1]
# mode = 1920x1080@60000
# position = 0,0
# transform = normal
# scale = 1.000000
#
# You can get the names of your outputs with wlr-randr.
# https://github.com/emersion/wlr-randr
#
# See also kanshi for configuring your outputs automatically.
# https://wayland.emersion.fr/kanshi/

# Core options ─────────────────────────────────────────────────────────────────

[core]

# List of plugins to be enabled.
# See the Configuration document for a complete list.
plugins = \
  autostart \
  command \
  decoration \
  fast-switcher \
  foreign-toplevel \
  grid \
  gtk-shell \
  idle \
  invert \
  move \
  oswitch \
  place \
  resize \
  shortcuts-inhibit \
  switcher \
  wayfire-shell \
  window-rules \
  wm-actions \
  zoom \
  simple-tile \
  follow-focus

# Close focused window.
close_top_view = <super> KEY_Q | <alt> KEY_F4

preferred_decoration_mode = server

xwayland = false 

focus_buttons_passthrough = true

[follow-focus]
change_view = true
focus_delay = 0

[decoration]
border_size = 2
# active_color = "#e1acff"
#active_color = 0.88 0.67 1.0 1.0
# darker now: #492971 (purple)
active_color = 0.29 0.16 0.44 1.0
font = mononoki NerdFont bold 13 
# inactive_color = "#222222"
inactive_color = 0.13 0.13 0.13 1.0
title_height = 22
ignore_views = none

[move]
# Drag windows by holding down Super and left mouse button.
activate = <super> BTN_LEFT

[simple-tile] 
button_move = <super>
button_resize = <super> <shift>
#keep_fullscreen_on_adjacent = false
key_toggle = BTN_MIDDLE
tile_by_default = true
inner_gap_size = 0

key_focus_above = <super> KEY_UP
key_focus_below = <super> KEY_DOWN
#key_focus_left = <super> KEY_J
key_focus_left = <super> KEY_LEFT
#key_focus_right = <super> KEY_K
key_focus_right = <super> KEY_RIGHT

# Resize them with right mouse button + Super.
[resize]
activate = <super> BTN_RIGHT

# Zoom in the desktop by scrolling + Super.
[zoom]
modifier = <super>

# Startup commands ─────────────────────────────────────────────────────────────

[autostart]
# Network
network = nm-applet 

# Set the wallpaper, start a panel and dock if you want one.
# https://github.com/WayfireWM/wf-shell
# background = wf-background
background = swaybg -m center -i ~/.local/share/backgrounds/1080p/mountain_rainfall-1080p.png
bar = waybar
#redshift = redshift -m wayland

# Brightness 
brighness = xbacklight 

# Syncthing 
syncthing = syncthing --no-browser 

# Output configuration
# https://wayland.emersion.fr/kanshi/
#outputs = kanshi

# Notifications
notifications = mako

# Screen color temperature
# https://sr.ht/~kennylevinsen/wlsunset/
#gamma = wlsunset

# Idle configuration
idle = swayidle before-sleep swaylock

# XDG desktop portal
# Needed by some GTK applications
portal = /usr/libexec/xdg-desktop-portal

[idle]
screensaver_timeout = 300
dpms_timeout = 600
# Disables the compositor going idle with Super + z.
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds.

# Applications ─────────────────────────────────────────────────────────────────

[command]

# Start a terminal
binding_terminal = <super> KEY_ENTER | <super> KEY_T
command_terminal = alacritty

# Screen locker
binding_screenlock = <super> KEY_L
command_screenlock = bash ~/.scripts/lockscreen.sh

# Logout
# https://github.com/ArtsyMacaw/wlogout
#binding_logout = <super> KEY_ESC
#command_logout = wlogout

# Screenshots
#binding_screenshot = <super> KEY_PRINT
binding_screenshot = <super> KEY_SYSRQ
command_screenshot = grim $HOME/Pictures/$(date '+%F_%T').png && aplay $HOME/.local/share/sounds/freesound_benboncan_dslr-click.wav
binding_screenshot_interactive = <super> <shift> KEY_S
command_screenshot_interactive = slurp | grim -g - $HOME/Pictures/$(date '+%F_%T').png && aplay $HOME/.local/share/sounds/freesound_benboncan_dslr-click.wav 

# Volume controls
# https://alsa-project.org
repeatable_binding_volume_up = KEY_VOLUMEUP
command_volume_up = amixer set Master 5%+
repeatable_binding_volume_down = KEY_VOLUMEDOWN
command_volume_down = amixer set Master 5%-
binding_mute = KEY_MUTE
command_mute = amixer set Master toggle

# Screen brightness (acpilight)
repeatable_binding_backlight_up = KEY_BRIGHTNESSUP
command_backlight_up = xbacklight -perceived -inc 5
repeatable_binding_light_down = KEY_BRIGHTNESSDOWN
command_backlight_down = xbacklight -perceived -dec 5

# Launching apps 
binding_vivaldi = <super> KEY_V 
#command_vivaldi = /usr/local/bin/vivaldi --enable-features=UseOzonePlatform --ozone-platform=wayland
# bubblewrap profile doesnt work with by current setup
command_vivaldi = /usr/bin/vivaldi --enable-features=UseOzonePlatform --ozone-platform=wayland

binding_obsidian = <super> KEY_Z
command_obsidian = obsidian --ozone-platform=wayland --ozone-platform-hint=auto --enable-features=UseOzonePlatform,WaylandWindowDecorations

binding_browser = <super> KEY_B 
command_browser = /usr/local/bin/firefox

binding_webbrowser = <super> KEY_W
command_webbrowser = /usr/local/bin/firefox

binding_torrent = <super> <shift> KEY_T 
command_torrent = deluge

binding_newsboat = <super> KEY_N 
command_newsboat = alacritty -e newsboat

# Launching directory
binding_music = <super> KEY_M 
command_music = pcmanfm -n ~/Music

binding_downloads = <super> KEY_D 
command_downloads = pcmanfm -n ~/Downloads

binding_documents = <super> <shift> KEY_D 
command_documents = pcmanfm -n ~/Documents

binding_filemanager = <super> KEY_F
command_filemanager = pcmanfm -n $HOME/

binding_pictures = <super> KEY_P
command_pictures = pcmanfm -n ~/Pictures

binding_practice = <super> <shift> KEY_P
command_practice = pcmanfm -n ~/Practice
# Launching menu
binding_powermenu = <alt> KEY_P 
command_powermenu = bash ~/.scripts/power_menu.sh

binding_launcher1 = <super> KEY_SPACE
command_launcher1 = bash ~/.scripts/search_menu.sh 

binding_launcher2 = <alt> KEY_R
command_launcher2 = bash ~/.scripts/run_prompt.sh 

binding_wayfire-config = <alt> KEY_E
command_wayfire-config = sh $HOME/.scripts/wayland-config_menu.sh

binding_emoji = <alt> <shift> KEY_E
command_emoji = BEMOJI_PICKER_CMD="wofi -d" bemoji

# Windows ──────────────────────────────────────────────────────────────────────

# Actions related to window management functionalities.
[wm-actions]
toggle_fullscreen = <alt> KEY_F
# toggle_always_on_top = <super> KEY_X
# toggle_sticky = <super> <shift> KEY_X
toggle_maximize = <alt> KEY_A
minimize = <alt> KEY_M
toggle_showdesktop = <alt> KEY_D

# Position the windows in certain regions of the output.
[grid]
#
# ⇱ ↑ ⇲   │ 7 8 9
# ← f →   │ 4 5 6
# ⇱ ↓ ⇲ d │ 1 2 3 0
# ‾   ‾
slot_bl = <super> KEY_KP1
slot_b = <super> KEY_KP2
slot_br = <super> KEY_KP3
slot_l = <super> KEY_LEFT | <super> KEY_KP4
slot_c = <super> KEY_UP | <super> KEY_KP5
slot_r = <super> KEY_RIGHT | <super> KEY_KP6
slot_tl = <super> KEY_KP7
slot_t = <super> KEY_KP8
slot_tr = <super> KEY_KP9
# Restore default.
activate_backward = <super> KEY_J
restore = <super> KEY_DOWN | <super> KEY_KP0

# Change active window with an animation.
[switcher]
next_view = <alt> KEY_TAB
prev_view = <alt> <shift> KEY_TAB

# Simple active window switcher.
[fast-switcher]
activate_backward = <super> KEY_J
activate = <super> KEY_K

# Outputs ──────────────────────────────────────────────────────────────────────

# Change focused output.
[oswitch]
# Switch to the next output.
next_output = <super> KEY_O
# Same with the window.
next_output_with_win = <super> <shift> KEY_O

# Invert the colors of the whole output.
[invert]
toggle = <super> KEY_I

# Send toggle menu event.
[wayfire-shell]
toggle_menu = <super>

# Rules ────────────────────────────────────────────────────────────────────────

# Example configuration:
#
#[window-rules]
# maximize_alacritty = on created if app_id is "Alacritty" then maximize
#
# You can get the properties of your applications with the following command:
# $ WAYLAND_DEBUG=1 alacritty 2>&1 | kak
