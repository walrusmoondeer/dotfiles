# You may need to manually set your language environment
export LANG=en_US.UTF-8
export CFG=$HOME/.config/

export BROWSER=vivaldi-stable
export EDITOR=nvim
export TERM=alacritty

#export HISTTIMEFORMAT="%Y-$m-%d %T " 
