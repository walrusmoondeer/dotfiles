# Executes a 'fetch' utility on terminal start-up
fastfetch
# macchina

# colored prompt might not work without this command.
autoload -U colors && colors

# Disable ctrl-s to freeze terminal.
stty stop undef

# tab completion
autoload -Uz compinit
zstyle ':comptetion:*' menu select
#zmodload zsh/compinit
compinit
_comp_optons+=(globdots)  # includes hidden files

# Show a color, spedicied with a hex value like this: '#ffffff', 
# /* or spawn default colors like this: 'red|blue|white|etc...'
color () {
  print -P %F{"${1}"}"${1}"
}

# History
export HISTFILE=~/.histfile
export HISTSIZE=1000
export SAVEHIST=1000

# automatically cd into a directory
#setopt autocd   

# dotfiles alias for git bear repository (for saving config files in github)
#alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# PROMPT STYLE
# luke smiths ps1 prombt style
export PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
# my PS1 style, with time added instead of host
#export PS1="[%n#%T %~]$ "
#export PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}#%{$fg[cyan]%}time:%{$fg[blue]%}%T %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# Continuation look:
#export PS2="$fg[magenta]> " # makes everything in continuation magenta colored

# Dry Nietzsche quotes
alias drynietzsche="fortune -s -n 400 $HOME/.config/fortune/nietzsche/fortune"
# Nietzsche quote generator (cowsay, colourful(lolcat))
alias nietzsche="drynietzsche | cowsay | lolcat"

# YouTube-dl 
alias yt-video="yt-dlp --embed-metadata --prefer-free-formats -S 'res:720,fps:25' -o '$HOME/Videos/yt-dl/%(creator)s - %(title)s - %(release_date)s.%(ext)s'"
alias yt-video-mp4="yt-dlp --embed-metadata --recode-video mp4 -o '$HOME/Videos/yt-dl/%(creator)s - %(title)s - %(release_date)s.%(ext)s'"
alias yt-music="bash $HOME/.scripts/yt-music.bash"
alias yt-book="yt-dlp --extract-audio --format wa --embed-thumbnail --convert-thumbnails jpg -o '$HOME/'Audio Books'/%(title)s.%(ext)s'"
# couldnt change OPUS quality, thus decided to use 'worst' quality, ideally i would use OPUS 32kbits

# Common command modifications
#alias ls="ls --color=auto"
alias ls="eza --color=always --icons"
#alias la="ls -a"
alias la="eza -a --color=always --icons"
#alias lt="ls --human-readable --size -1 -S --classify"  # ls with file sizes and organised by size
alias ll="eza -lh --color=always --color-scale --icons --no-user"  ## --no-time --no-permissions
alias rm="rm -i"
alias mv="mv -i"
function cd {  # everytime you cd into somewhere, you will automattically ls
    builtin cd "$@" && eza -a --color=always --icons
    }

# Shortcuts
alias P="cd $HOME/Practice"
alias bd="cd .."
alias myip="curl ipinfo.io/ip"  # check my public ip
alias localip="ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p'"
alias untar="tar -zxvf"
alias nb="newsboat"
alias nf="fastfetch"
alias D="cd $HOME/Downloads"
alias Doc="cd $HOME/Documents"
alias pig='du -cks * | sort -rn | head -11'
alias code="codium --no-sandbox --unity-launch"
alias exif="exiv2"
alias passwd-check="zxcvbn"

# Package management:
alias upd="doas pacman -Syu" # alias upd="doas pacman -Syu --ignore=awesome"
alias R="yay -Rns"
alias S="yay -Ss"
alias Si="yay -Sii"
alias I='yay -S'
alias cleanup='doas pacman -Rns $(pacman -Qtdq);doas pacman -Scc;doas rm -rf $HOME/.cache/yay/*' # remove orphaned and cached packages
alias pkglist="yay -Qqe"

# Trash-cli:
# One of the trash locations: $HOME/.local/share/Trash/files/***
alias rmv="trash-put"
#  trash-restore       restore a trashed file.
#  trash-rm            remove individual files from the trashcan.

# Edit Config aliases
alias zshrc="$EDITOR $ZDOTDIR/.zshrc"
alias nbrc="$EDITOR $HOME/.config/newsboat/urls"  # edit newsboat urls
alias cfg="cd $HOME/.config"

# Colored Manpages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
## What each code means
# 31 - red
# 32 - green
# 33 - yellow
# 0 - reset/normal
# 1 - bold 
# 4 - underlined
# https://www.howtogeek.com/683134/how-to-display-man-pages-in-color-on-linux/

# Enabling Zsh plugins
source $HOME/.config/zsh/scripts/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/.config/zsh/scripts/zsh-autosuggestions/zsh-autosuggestions.zsh 
source $HOME/.config/zsh/scripts/zsh-autopair/autopair.zsh
